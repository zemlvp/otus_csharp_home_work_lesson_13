﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace otus_csharp_home_work_lesson_13
{
    public class FileArgs : EventArgs
    {
        public string filePath;
        public FileArgs(string filePath)
        {
            this.filePath = filePath;
        }
    }

    public class Folder
    {
        private string folderPath { get; set; }
        public event EventHandler<FileArgs> FileFound;

        public Folder(string folderPath)
        {
            this.folderPath = folderPath;
        }

        public void OnFileFound(string filePath)
        {
            FileArgs fileArgs = new FileArgs(filePath);

            if (FileFound != null)
                FileFound(this, fileArgs);
        }

        public void Iterate()
        {
            var filesPath = Directory.GetFileSystemEntries(folderPath, "*", SearchOption.AllDirectories);
            foreach (var filePath in filesPath)
            {
                FileInfo fi = new FileInfo(filePath);
                if (fi.Attributes != FileAttributes.Directory)
                    FileFound?.Invoke(this, new FileArgs(filePath));
            }
        }
    }
}
