﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace otus_csharp_home_work_lesson_13
{
    public static class Exts
    {
        public static T GetMax<T>(this IEnumerable<T> list, Func<T, float> getParametr) where T : class
        {
            return list.Where(a => getParametr(a) == list.Max(a => getParametr(a))).FirstOrDefault();
        }
    }

    class TestClass
    {
        public int I { get; set; }
        public float J { get; set; }

        public TestClass(int i, float j)
        {
            I = i;
            J = j;
        }
    }

    class Program
    {
        private static readonly TestClass[] testClass = new TestClass[10];

        static void Main()
        {
            // 1. Написать обобщённую функцию расширения, находящую и возвращающую максимальный элемент коллекции.
            // Функция должна принимать на вход делегат, преобразующий входной тип в число для возможности поиска максимального значения.
            // public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class; 
            Task1();

            Console.WriteLine(new string('-', 80));

            // 2. Написать класс, обходящий каталог файлов и выдающий событие при нахождении каждого файла
            //Task2();

            Console.ReadLine();
        }

        private static void Task1()
        {
            Console.WriteLine("-- Задача 1. --");

            InitTestClass();

            var maxTestClassI = testClass.GetMax(x => x.I);

            foreach (var a in testClass)
                Console.WriteLine(a.I);

            Console.WriteLine("maxTestClassI={0}", maxTestClassI.I);

            Console.WriteLine();

            var maxTestClassJ = testClass.GetMax(x => x.J);

            foreach (var a in testClass)
                Console.WriteLine(a.J);

            Console.WriteLine("maxTestClassJ={0}", maxTestClassJ.J);
        }

        private static void InitTestClass()
        {
            testClass[0] = new TestClass(11, 0);
            testClass[1] = new TestClass(1, 0.1f);
            testClass[2] = new TestClass(-3, 56.4f);
            testClass[3] = new TestClass(45, 7.77f);
            testClass[4] = new TestClass(11, 45f);
            testClass[5] = new TestClass(-1, 0.001f);
            testClass[6] = new TestClass(7, 4.5f);
            testClass[7] = new TestClass(3, 56f);
            testClass[8] = new TestClass(188, 43434.44f);
            testClass[9] = new TestClass(99, 0);
        }

        private static void Task2()
        {
            Console.WriteLine("-- Задача 2. --");

            var folder = new Folder(@"C:\Projects\Beeline\Билайн");
            folder.FileFound += FileFoundHandler;
            folder.Iterate();
        }

        private static void FileFoundHandler(object sender, FileArgs e)
        {
            if (e.filePath.Contains("test"))
            {
                ((Folder)sender).FileFound -= FileFoundHandler;
                return;
            }

            Console.WriteLine(e.filePath);
        }
    }
}
